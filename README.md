# US Wildfires Project

This is a quick project consisting of an index page showing a list of forests and an information page showing information 
about a selected forest and the number of fires that have occurred at the location. 

## Running the Project

After downloading the project, from the root of the project, please run the following commands: 

Copy the .env.example file to .env:

``` cp .env.example .env ``` (Linux or Mac)

``` copy .env.example .env ``` (Windows)

Install PHP dependencies:

``` composer install ``` 

Generate app key:

```php artisan key:generate```

Install javascript dependencies: 

``` npm install ```

Serve application: 

``` php artisan serve ```

Composer, nodejs and npm are requirements for this project, the required sqlite driver must also be downloaded and 
enabled. The application assumes the database file to be located at /database/database.sqlite

