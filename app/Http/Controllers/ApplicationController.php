<?php

namespace App\Http\Controllers;

use App\Models\Forest;
use Illuminate\View\View;

class ApplicationController extends Controller
{
    public function index(): View {

        $name = request()->query("name");
        $area = request()->query("area");
        $minFires = request()->query("minFires");
        $maxFires = request()->query("maxFires");

        $forests = $area != null ? Forest::where("GeographicArea", 'like', "%$area%") : Forest::orderBy("GeographicArea");

        if($name != null) $forests = $forests->where("name", "like", "%$name%");
        if($minFires != null) $forests = $forests->has("fires", ">=", $minFires);
        if($maxFires != null) $forests = $forests->has("fires", "<=", $maxFires);

        return view('forests.index', ['forests' => $forests->with("fires")->paginate(50)]);
    }

    public function view($id): View {
        return view('forests.show', ['forest' => Forest::with('fires')->findOrFail($id)]);
    }

}
