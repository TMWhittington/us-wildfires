<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Forest extends Model
{
    use HasFactory;

    protected $table = 'NWCG_UnitIDActive_20170109';

    protected $primaryKey = 'UnitId';
    public $incrementing = false;
    protected $keyType = 'string';

    public function fires(): Relation {
        return $this->hasMany(Fire::class, 'NWCG_REPORTING_UNIT_ID', 'UnitId');
    }
}
