@extends('templates.layout')

@section('title')
    Forests - {{  config("app.name") }}
@endsection

@section('content')

    <div class="col-12">

        <div class="spacer my-5"></div>

        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col col-sm-6"><h4>Forests</h4></div>
                    <div class="col col-sm-6">
                        <div class="p-3 import-search-container d-flex">
                            <select class="forest-search-select form-select">
                                <option value="name">Name</option>
                                <option value="area">Area</option>
{{--                                <option value="minFires">Minimum Fires</option>--}}
{{--                                <option value="maxFires">Maximum Fires</option>--}}
                            </select>
                            <input type="text" class="form-control forest-search-query"
                                   placeholder="Search">
                            <button class="btn btn-success forest-search-button">Search</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped table-responsive" id="datatable" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Area</th>
                                <th>Reported Fires</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($forests as $forest)

                                <tr>
                                    <td><span class="d-none table-field-label"></span> {{$forest->Name}}</td>
                                    <td><span class="d-none table-field-label"></span> {{$forest->GeographicArea}}</td>
                                    <td><span class="d-none table-field-label"></span> {{$forest->fires->count()}}</td>
                                    <td><a href="/forests/{{$forest->UnitId}}" class="btn btn-success text-white"
                                           title="view">More Information</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    {!! $forests->links() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('footer')
    <div id="ModalArea"></div>

    <script>
        window.addEventListener("DOMContentLoaded", () => {

            const input = document.querySelector(".forest-search-query");
            const type = document.querySelector(".forest-search-select");
            const button = document.querySelector(".forest-search-button");

            button.addEventListener("click", () => {
                console.log("Clicked")
                window.location.href = `${location.origin}${location.pathname}?${type.value}=${input.value}`
            });

        })
    </script>

@endpush
