@extends('templates.layout')

@section('title')
{{$forest->Name}} | Forests - {{  config("app.name") }}
@endsection


@section('content')

    <div class="col-sm-12">

        <div class="spacer my-5"></div>

        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <h4 class="card-title">{{$forest->Name}}, {{$forest->GeographicArea}}</h4>
                    <a href="{{URL::previous()}}" class="previous">Back</a>
                </div>
            </div>
            <div class="card-content mt-5">

                <div class="col col-md-6">
                    <h5 class="ms-2 mb-4">Overview</h5>
                    <table class="table table-responsive">
                        <tr>
                            <td><p>Wildland Role</p></td>
                            <td>{{$forest->WildlandRole}}</td>
                        </tr>
                        <tr>
                            <td><p>Unit Type</p></td>
                            <td>{{$forest->UnitType}}</td>
                        </tr>
                        <tr>
                            <td><p>Number of Fires</p></td>
                            <td>{{$forest->fires->count()}}</td>
                        </tr>
                    </table>
                </div>
                <div class="col col-md-6"></div>
            </div>

            @if($forest->fires->count() > 0)

                <div class="card">

                    <div class="card-header">
                        <div class="col-md-12">
                            <h5 class="card-title">Fires</h5>
                        </div>
                    </div>

                    <div class="card-content">
                        <div class="col">
                            <table class="table table-responsive" style="padding:40px 20px 0px 20px;">
                                <tr>
                                    <th>Date</th>
                                    <th>FPA_ID</th>
                                    <th>Name</th>
                                    <th>Cause</th>
                                </tr>
                                @foreach($forest->fires as $fire)
                                    <tr>
                                        <td>{{(new \Illuminate\Support\Carbon(sprintf("first day of January %s", $fire->FIRE_YEAR)))->addDays($fire->DISCOVERY_DOY)->format("Y-m-d") }}</td>
                                        <td>{{$fire->FPA_ID}}</td>
                                        <td>{{$fire->FIRE_NAME}}</td>
                                        <td>{{$fire->STAT_CAUSE_DESCR}}</td>
                                    </tr>
                                @endforeach

                            </table>

                        </div>
                    </div>

                </div>



                @endif
        </div>
    </div>
    </div>

    <style>

        .payout-overview p {
            color: grey;

        }

        .payout-overview, .payout-overview tr, .payout-overview td {
            border: none !important;
        }

        .payout-overview td {
            padding: 2px !important;
        }


    </style>


@endsection
