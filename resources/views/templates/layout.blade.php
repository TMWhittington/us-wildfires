<!doctype html>
<html lang="en">
<head>
    @vite(['resources/sass/app.scss', 'resources/js/app.ts'])
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield("title") | {{ config("app.name") }}</title>

</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-success" id="mainNav">
    <div class="container">
        <a class="navbar-brand text-white" href="/"><img style="height:3em; margin-right: 2em;" src="{{Vite::asset('resources/images/owl.webp')}}" alt="..." /> Hiking App</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ms-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">

            </ul>
        </div>
    </div>
</nav>

<div class="content">

    @yield("content")
</div>

<!-- Footer-->
<footer class="footer py-4 bg-success text-white">
    <div class="container">
        <div class="row align-items-center">
            <div class="text-center">Copyright &copy; {{ config("app.name") }} {{date("Y")}}</div>
        </div>
    </div>
</footer>

@stack("footer")
</body>
</html>
